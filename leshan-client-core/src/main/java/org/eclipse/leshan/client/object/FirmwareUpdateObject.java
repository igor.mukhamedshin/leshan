package org.eclipse.leshan.client.object;

import java.util.UUID;

import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.client.servers.ServerIdentity;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.request.argument.Arguments;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;

public class FirmwareUpdateObject extends BaseInstanceEnabler {

    @Override
    public WriteResponse write(ServerIdentity identity, boolean replace, int resourceid, LwM2mResource value) {
        return WriteResponse.success();
    }

    @Override
    public synchronized ReadResponse read(ServerIdentity identity, int resourceId) {
        if (resourceId == 3) {
            return ReadResponse.success(resourceId, 2);
        }
        if (resourceId == 5) {
            return ReadResponse.success(resourceId, 1);
        }
        return ReadResponse.success(resourceId, UUID.randomUUID().toString());
    }

    @Override
    public synchronized ExecuteResponse execute(ServerIdentity identity, int resourceId, Arguments arguments) {
        return ExecuteResponse.success();
    }
}
